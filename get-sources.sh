#!/bin/sh
#
# This script fetches sources ... 
#
# Usage:
#
#     @SCRIPT@ [-x] [-r <src_root>] [-t <tag>]
#     @SCRIPT@ -h
#
#     -h                print this message and exit
#
#     -x                debug mode
#
#     -t <tag>          update sources to the tag
#
#     -r <src_root>     path to the root OJDK sources dir
#                       default is ./openjdk
#

MYDIR=$(sh -c "cd $(dirname $0) && pwd")
MYNAME=$(basename $0)

export LC_ALL=C
TEMPDIR=
KEEP_TMP=no

fail() {
   printf "${red}ERROR: $@ ${no_color}\n" >&2
   exit 1
}

usage() {
   cat ${MYDIR}/${MYNAME} | sed -n '/^#$/,/^$/p' | sed -e "s/@SCRIPT@/${MYNAME}/g" -e 's/^#//'
   exit 0
}

on_exit() {
   # Remove temp directory
   if [ "${KEEP_TMP}" = "no" -a -d "${TEMPDIR}" ]; then
      rm -rf ${TEMPDIR}
   fi
   [ -f "${PID_FILE}" ] && /bin/rm -f "${PID_FILE}"
}

on_int() {
   if [ -f ${PID_FILE} ]; then
      echo "Script interrupted. Killing subprocesses..."
      PIDS=$(cat ${PID_FILE})
      rm -f ${PID_FILE}
      if [ -n "${PIDS}" ]; then
         PIDS=$(ps axo pid,ppid | egrep `echo ${PIDS} | sed 's/ /|/g'` | xargs -n1 echo | sort -u)
         echo ${PIDS} | xargs -n 1 kill -TERM 2>/dev/null
         wait
      fi
   fi
   rm -f /tmp/$$.*
}

trap on_int INT TERM
trap on_exit EXIT

clone_or_pull() {
   local DIR=$1
   local URL=$2
   local TAG=$3
   local REPO=$(basename $DIR)

   mkdir -p ${DIR}
   if [ -d ${DIR}/.hg ]; then
      printf "${blue}Updating ${DIR} from ${URL} to ${green}${TAG}${blue} ...${no_color}\n"
      CMD=pull
   else
      printf "${blue}Cloning ${DIR} from ${URL} to ${green}${TAG}${blue} ...${no_color}\n"
      CMD="clone -U"
   fi

   ((
   ${HG} --cwd ${DIR} ${CMD} ${URL} .
   [ $? -eq 0 ] || fail "${CMD} from ${URL} failed!"
   ${HG} --cwd ${DIR} up ${TAG}
   [ $? -eq 0 ] || fail "${REPO} update failed!"
   ) > /tmp/$$.$REPO.log 2>&1; cat /tmp/$$.$REPO.log; rm /tmp/$$.$REPO.log)&
   echo $! >> ${PID_FILE}
}

while getopts ":hr:xt:" opt; do
   case $opt in
      h) usage ;;
      r) OJDK_ROOT=$OPTARG ;;
      t) HGTAG=$OPTARG ;;
      x) DEBUG=yes ;;
      :) fail "Option -$OPTARG requires an argument." ;;
      *) fail "Unrecognized option -$OPTARG." ;;
   esac
done

shift $(expr $OPTIND - 1)

PID_FILE=$(mktemp /tmp/${MYNAME}.XXXXXX)

if tty -s; then
   no_color='\033[0m'
   red='\033[0;31m'
   green='\033[32m'
   blue='\033[34m'
fi

[ "$DEBUG" = "yes" ] && set -x

# REPO_OL=git:https://bitbucket.org/ipk/objectlayoutlibrary.git
REPO_OPENJDK=http://hg.openjdk.java.net/jdk8u/jdk8u-dev

REPO_CORBA=${REPO_OPENJDK}/corba
REPO_JAXP=${REPO_OPENJDK}/jaxp
REPO_JAXWS=${REPO_OPENJDK}/jaxws
REPO_LANGTOOLS=${REPO_OPENJDK}/langtools
REPO_NASHORN=${REPO_OPENJDK}/nashorn

REPO_HOTSPOT=https://bitbucket.org/ipk/structuredarray-hotspot
REPO_JDK=https://bitbucket.org/ipk/structuredarray-jdk

HG=$(which hg)

OJDK_ROOT=${OJDK_ROOT:-${MYDIR}/openjdk}
HGTAG=${HGTAG:-default}

mkdir -p ${OJDK_ROOT}

# Clone root openjdk directory
clone_or_pull ${OJDK_ROOT} ${REPO_OPENJDK} ${HGTAG}
wait

# Get repositories list from hgforest.sh script
REPOS="corba jaxp jaxws langtools nashorn"

for repo in ${REPOS}; do
   REPO=$(echo ${repo} | tr 'a-z' 'A-Z')
   clone_or_pull ${OJDK_ROOT}/${repo} $(eval echo \$"REPO_$REPO") ${HGTAG}
done

for repo in hotspot jdk; do
   REPO=$(echo ${repo} | tr 'a-z' 'A-Z')
   clone_or_pull ${OJDK_ROOT}/${repo} $(eval echo \$"REPO_$REPO")
done


wait






