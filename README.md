### Layout of related repositories ###

See https://bitbucket.org/ipk/structuredarray-hotspot for details

### How to build and test Intrinsified Object Layout ###

10 Simple steps

1. git clone https://bitbucket.org/ipk/objectlayoutlibrary
1. cd to objectlayoutlibrary
1. run sh get-sources.sh to obtain your copy of the custom OpenJDK8
1. cd to openjdk
1. run sh ./configure. Refer to OpenJDK's readme file for details
1. run make images to build binaries for the custom jdk
1. set OL_JAVA_HOME env variable to point to your new jdk image. Default is openjdk/build/images/j2sdk-image/
1. cd back to objectlayoutlibrary directory
1. run make install to build IntrinsifiedObjectLayout*.jar library and install it into your local maven repository
1. run make test to run our set of tests