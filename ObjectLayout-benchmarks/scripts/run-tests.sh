#!/bin/sh


RUN=run01

logfile=".20150624.${RUN}."


OL_JAVA_HOME=/home/ivan/ol_july2015/ol.j2sdk
OJ_JAVA_HOME=/home/ivan/ol_july2015/8u45b14.j2sdk


XMX1="-Xms1g -Xmx1g"
XMX4="-Xms4g -Xmx4g"
XMX8="-Xms8g -Xmx8g"
XMX15="-Xms15g -Xmx15g"

PROFILING_PREFIX_ON=" -Djmh.perfasm.events=cycles,cache-misses "
PROFILING_SUFFIX_ON=" -i 1 -wi 1 -wf 0 -f 0  -prof perfasm "
PROFILING_PREFIX_OFF=" "
PROFILING_SUFFIX_OFF=" -i 1 -wi 0 -wf 0 -f 0 "


PRINTER="echo "

COMMON_OPTIONS=" -XX:-TieredCompilation -XX:+UseParallelGC -XX:+UseParallelOldGC -XX:-UseCompressedOops -d64 -server "
OJ_OPTIONS=" -jar /home/ivan/ol_july2015/ObjectLayout/ObjectLayout-benchmarks/target/benchmarks.jar"
OL_OPTIONS=" -Xbootclasspath/p:/home/ivan/.m2/repository/org/objectlayout/ObjectLayoutIntrinsified/1.0.5-SNAPSHOT/ObjectLayoutIntrinsified-1.0.5-SNAPSHOT.jar -XX:+UseObjectLayoutIntrinsics  -jar /home/ivan/ol_july2015/objectlayoutlibrary/ObjectLayout-benchmarks//target/benchmarks.jar"


#TESTS="arrayLoopSumTest loopEncapsulatedArraySumTest loopEncapsulatedRandomizedArraySumTest loopGenericEncapsulatedArraySumTest subclassedArrayLoopSumTest"
TESTS="structuredArrayLoopTest subclassedStructuredArrayLoopTest genericArrayLoopTest basicArrayLoopTest randomizedBasicArrayLoopTest"


#turn profiling with jmh's perfasm plugin on or off

for TOUGLE in OFF; do


# turn number of gigabytes for java heap
  for DIGIT in 2; do


#   vanilla openjdk or ObjeclLayout's Openjdk
    for JAVA in OJ OL; do


#     subtests of the benchmark
      for test in $TESTS ; do


#       in a loop access array elements randomly or sequentially
        for randomWalk in true false ; do


#       in a loop do a simple xor on element and accumulator or do lots of math to interleave memory operations
        for SimpleMath in true false ; do


#       instantiale all java arrays with Structured Arrays'elements to guarantee same pattern of mem access or not
        for SameLayout in true false ; do

              JAVA_HOME_VAR=${JAVA}_JAVA_HOME
              eval JAVA_HOME_RUN=\$${JAVA_HOME_VAR}

              PROFILING_PREFIX_VAR=PROFILING_PREFIX_${TOUGLE}
              eval PROFILING_PREFIX=\$${PROFILING_PREFIX_VAR}

              PROFILING_SUFFIX_VAR=PROFILING_SUFFIX_${TOUGLE}
              eval PROFILING_SUFFIX=\$${PROFILING_SUFFIX_VAR}

              XMX_VAR=XMX${DIGIT}
              eval XMX=\$${XMX_VAR}

              OPTIONS_VAR=${JAVA}_OPTIONS
              eval OPTIONS=\$${OPTIONS_VAR}

              JMH_PARAMS=" -p RandomArrayWalk="${randomWalk}"  -p SameLayout="${SameLayout}" -p SimpleMath="${SimpleMath}

              TO="-e  bench.ObjectLayoutBench.${test}   bench.ObjectLayoutBench.${test}"

              echo               ${JAVA_HOME_RUN}/bin/java  ${PROFILING_PREFIX} ${COMMON_OPTIONS} ${XMX} ${OPTIONS}  ${PROFILING_SUFFIX} ${JMH_PARAMS} ${TO} 2>&1 | tee ${JAVA}${logfile}xmx${DIGIT}g.rw_${randomWalk}.sm_${SimpleMath}.sl_${SameLayout}.${test}.perfs_${TOUGLE}


              ${JAVA_HOME_RUN}/bin/java  ${PROFILING_PREFIX} ${COMMON_OPTIONS} ${XMX} ${OPTIONS}  ${PROFILING_SUFFIX} ${JMH_PARAMS} ${TO} 2>&1 | tee ${JAVA}${logfile}xmx${DIGIT}g.rw_${randomWalk}.sm_${SimpleMath}.sl_${SameLayout}.${test}.perfs_${TOUGLE}
        done
        done
        done
      done
    done
  done
done



# turn number of gigabytes for java heap
  for DIGIT in 2; do


# in a loop access array elements randomly or sequentially
  for randomWalk in true false ; do


# in a loop do a simple xor on element and accumulator or do lots of math to interleave memory operations
  for SimpleMath in true false ; do


# instantiale all java arrays with Structured Arrays'elements to guarantee same pattern of mem access or not
  for SameLayout in true false; do



  echo "=== Case: Simple math is ${SimpleMath}  Same Layout is ${SameLayout}  Random access is ${randomWalk} ==="
  echo "|| Benchmark || Vanilla JDK || Intrinsified JDK ||"

#     subtests of the benchmark
      for test in $TESTS ; do

        ol_lastline=$(tail -n 1  OL${logfile}xmx${DIGIT}g.rw_${randomWalk}.sm_${SimpleMath}.sl_${SameLayout}.${test}.perfs_${TOUGLE})
        ol_val="$(echo $ol_lastline | awk '{print $8}')"
        ol_err="$(echo $ol_lastline | awk '{print $9}')"
              
        oj_lastline=$(tail -n 1  OJ${logfile}xmx${DIGIT}g.rw_${randomWalk}.sm_${SimpleMath}.sl_${SameLayout}.${test}.perfs_${TOUGLE})
        oj_val="$(echo $oj_lastline | awk '{print $8}')"
        oj_err="$(echo $oj_lastline | awk '{print $9}')"
               

        echo "|| ${test} || ${ol_val} || ${ol_err} || ${oj_val} || ${oj_err} ||" 
      done
      echo 
      echo 
      echo 
   done
   done
   done
   done




