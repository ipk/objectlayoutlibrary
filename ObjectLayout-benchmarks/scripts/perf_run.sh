#!/bin/sh


OL=OL
OJ=OJ
OR=OR
logfile=".20150520.run03."


OL_JAVA_HOME=/home/ivan/ol_may2015/images/j2sdk-image/
OJ_JAVA_HOME=/opt/zulu8u45
OR_JAVA_HOME=/opt/jdk1.8.0_45


XMX1="-Xms1g -Xmx1g"
XMX4="-Xms4g -Xmx4g"
XMX8="-Xms8g -Xmx8g"
XMX15="-Xms15g -Xmx15g"

PROFILING_PREFIX_ON=" -Djmh.perfasm.events=cycles,cache-misses " 
PROFILING_SUFFIX_ON=" -f 1 -prof perfasm "
PROFILING_PREFIX_OFF=" " 
PROFILING_SUFFIX_OFF=" "

JMH_PARAMS="  -i 3 -wi 1 -wf 1 -f 3"


PRINTER=""

COMMON_OPTIONS=" -XX:NewRatio=2 -XX:-TieredCompilation -XX:+UseParallelGC -XX:+UseParallelOldGC -XX:-UseCompressedOops -d64 -server "
OJ_OPTIONS=" -jar /home/ivan/ol_may2015/ObjectLayout.vanilla/ObjectLayout-benchmarks/target/benchmarks.jar"
OL_OPTIONS=" -Xbootclasspath/p:/home/ivan/.m2/repository/org/objectlayout/ObjectLayoutIntrinsified/1.0.5-SNAPSHOT/ObjectLayoutIntrinsified-1.0.5-SNAPSHOT.jar -XX:+UseObjectLayoutIntrinsics  -jar /home/ivan/ol_may2015/objectlayoutlibrary/ObjectLayout-benchmarks//target/benchmarks.jar"
OR_OPTIONS=${OJ_OPTIONS}


for TOUGLE in OFF; do
  for DIGIT in 8; do
    for JAVA in OJ OL; do
      JAVA_HOME_VAR=${JAVA}_JAVA_HOME
      eval JAVA_HOME=\$${JAVA_HOME_VAR}
      PROFILING_PREFIX_VAR=PROFILING_PREFIX_${TOUGLE}
      eval PROFILING_PREFIX=\$${PROFILING_PREFIX_VAR}
      PROFILING_SUFFIX_VAR=PROFILING_SUFFIX_${TOUGLE}
      eval PROFILING_SUFFIX=\$${PROFILING_SUFFIX_VAR}
      XMX_VAR=XMX${DIGIT}
      eval XMX=\$${XMX_VAR}
      OPTIONS_VAR=${JAVA}_OPTIONS
      eval OPTIONS=\$${OPTIONS_VAR}


      echo ${JAVA_HOME}/bin/java  ${PROFILING_PREFIX} ${COMMON_OPTIONS} ${XMX} ${OPTIONS} ${JMH_PARAMS} ${PROFILING_SUFFIX} 2>&1 | tee ${JAVA}${logfile}xmx${DIGIT}g.perfs_${TOUGLE}
      ${PRINTER} ${JAVA_HOME}/bin/java  ${PROFILING_PREFIX} ${COMMON_OPTIONS} ${XMX} ${OPTIONS} ${JMH_PARAMS} ${PROFILING_SUFFIX} 2>&1 | tee ${JAVA}${logfile}xmx${DIGIT}g.perfs_${TOUGLE}
    done
  done
done


