package bench;

import java.lang.reflect.Constructor;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.ObjectLayout.ConstructionContext;
import org.ObjectLayout.CtorAndArgs;
import org.ObjectLayout.CtorAndArgsProvider;
import org.ObjectLayout.StructuredArray;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;


/*

  This benchmark creates a 1-D array of structures in 5 different ways
  - Java array of structs
  - Java array declared with the use of generic
  - Hava array with shuffled elements - to mimic layout of elements in arrays after many GC cycles
                                           when elements are referenced from multiple objects.
                                        In real life application we are more likely to see partial re-order
  - Structured Arrray
  - A subclass of Strructured Array

  For all 5 arrays this benchmark loops over all elements and does some operation. The reported score in number of
  operation per second, higher is better.

  There are several parameters to tune this benchmark

  length - number of elements in each array

  SamaLayout - when true, Structured Array is created first, and then all java arrays are initialized by the references
  to elements of Strcutured Array. This insures that memory access pattern is the same for all 5 benchmark loops and
  the only diffrence to be observed is how JIT transforms the main benchmark loop in each case.  When false, each of
  the 5 arrays is created independently.

  RandomArrayWalk - when true, the main loop accesses elements in array is a random order, otherwise elements are read
  sequentially. The main loop does this:  for (...) { val = array.get(index).val ; index = val + 1;}
  In a RandomArrayWalk case the val would always be a random number between 0 and length -1; otherwise
  array.get(index).val == index

  SimpleMath - when true, the body of a loop does the simplest math: accumulator ^= array.get(index).val, otherwise
  the math is more complex to interleave math operations with memory loads, thus making benchmark less memory bound.

  Run all benchmarks:
    $ java -jar target/benchmarks.jar

  Run selected benchmarks:
    $ java -jar target/benchmarks.jar (regexp)

  Run the profiling (Linux only):
     $ java -Djmh.perfasm.events=cycles,cache-misses -jar target/benchmarks.jar -f 1 -prof perfasm
 */
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(5)
@State(Scope.Thread)



public class ObjectLayoutBench {

// Default value is 1048576 = 2 ^ 20 or ~ 1M

    @Param({"1048576"})
    public long length;

    @Param({"true"})
    public boolean SameLayout;

    @Param({"false"})
    public boolean RandomArrayWalk;

    @Param({"true"})
    public boolean SimpleMath;

    StructuredArray<MockStructure> structuredArray;
    StructuredArrayOfMockStructure structuredArraySubclassed;
    GenericEncapsulatedArray<MockStructure> encapsulatedArrayGeneric;
    EncapsulatedArray encapsulatedArray;
    EncapsulatedRandomizedArray encapsulatedArrayRandomized;

    public static int long2Int(long l) {
        return (int) Math.max(Math.min(Integer.MAX_VALUE, l), Integer.MIN_VALUE);
    }

    @Setup
    public void setup() throws NoSuchMethodException {

        final Object[] args = new Object[3];
        final CtorAndArgs<MockStructure> ctorAndArgs =
                new CtorAndArgs<MockStructure>(
                            MockStructure.class.getConstructor(MockStructure.constructorArgTypes_Long_Bool_Int), args);

        if (MockStructure.class.getConstructor(MockStructure.constructorArgTypes_Long_Bool_Int) == null) {
            System.out.println("Failed to get constructor");
        }

        final CtorAndArgsProvider<MockStructure> ctorAndArgsProvider;
        ctorAndArgsProvider =
                new CtorAndArgsProvider<MockStructure>() {
                    @Override
                    public CtorAndArgs<MockStructure> getForContext(
                            ConstructionContext<MockStructure> context) throws NoSuchMethodException {
                        args[0] = context.getIndex();
                        args[1] = RandomArrayWalk;
                        args[2] = (int)length;
                        return ctorAndArgs;
                    }
                };

        structuredArray = StructuredArray.newInstance(MockStructure.class, ctorAndArgsProvider, length);
        structuredArraySubclassed = StructuredArrayOfMockStructure.newInstance(ctorAndArgsProvider, length);

        if (SameLayout) {

            // use Sructured Array to initialize java arrays

            encapsulatedArray = new EncapsulatedArray(structuredArray);
            encapsulatedArrayRandomized = new EncapsulatedRandomizedArray(structuredArray);
            encapsulatedArrayGeneric =
                    new GenericEncapsulatedArray<MockStructure>(
                            MockStructure.class.getConstructor(MockStructure.constructorArgTypes_SA), structuredArray);
        } else {
            encapsulatedArray = new EncapsulatedArray(long2Int(length), RandomArrayWalk);
            encapsulatedArrayRandomized = new EncapsulatedRandomizedArray(long2Int(length), RandomArrayWalk);
            encapsulatedArrayGeneric =
                new GenericEncapsulatedArray<MockStructure>(
                        MockStructure.class.getConstructor(MockStructure.constructorArgTypes_Long_Bool_Int),
                        long2Int(length), RandomArrayWalk);
        }
    }

    // TODO: We should probably sink the values into Blackhole.consume,
    // instead of summing them up, and subjecting ourselves with loop optimizations.

    @Benchmark
    public long structuredArrayLoopTest() {
        long accumulator = 0;
        int index = 0 ;
        final long length = structuredArray.getLength();

        if (SimpleMath) {
          for (long i = 0 ; i < length; i++) {
            int test_val= structuredArray.get(index).getTestValue();
            accumulator = MockStructure.simple_math(accumulator, test_val);
            index = test_val;
          }
        } else {
          for (long i = 0 ; i < length; i++) {
            int test_val= structuredArray.get(index).getTestValue();
            accumulator = MockStructure.some_math(accumulator, test_val);
            index = test_val;
          }
        }
        return accumulator;
    }

    @Benchmark
    public long subclassedStructuredArrayLoopTest() {
        long accumulator = 0;
        int index = 0 ;
        final long length = structuredArraySubclassed.getLength();
        if (SimpleMath) {
          for (long i = 0 ; i < length; i++) {
            int test_val= structuredArraySubclassed.get(index).getTestValue();
            accumulator = MockStructure.simple_math (accumulator, test_val);
            index = test_val ;
          }
        } else {
          for (long i = 0 ; i < length; i++) {
            int test_val= structuredArraySubclassed.get(index).getTestValue();
            accumulator = MockStructure.some_math(accumulator, test_val);
            index = test_val ;
          }
        }
        return accumulator;
    }

    @Benchmark
    public long genericArrayLoopTest() {
        long accumulator = 0;
        int index = 0 ;
        final int length = encapsulatedArrayGeneric.getLength();
        if (SimpleMath) {
          for (int i = 0 ; i < length; i++) {
            int test_val = encapsulatedArrayGeneric.get(index).getTestValue();
            accumulator = MockStructure.simple_math(accumulator, test_val);
            index = test_val;
          }
        } else {
          for (int i = 0 ; i < length; i++) {
            int test_val= encapsulatedArrayGeneric.get(index).getTestValue();
            accumulator = MockStructure.some_math(accumulator, test_val);
            index = test_val;
          }
        }
        return accumulator;
    }

    @Benchmark
    public long basicArrayLoopTest() {
        long accumulator = 0;
        int index = 0 ;
        final int length = encapsulatedArray.getLength();
        if (SimpleMath) {
          for (int i = 0 ; i < length; i++) {
            int test_val=encapsulatedArray.get(index).getTestValue();
            accumulator = MockStructure.simple_math (accumulator, test_val);
            index = test_val;
          }
        } else {
          for (int i = 0 ; i < length; i++) {
            int test_val=encapsulatedArray.get(index).getTestValue();
            accumulator = MockStructure.some_math(accumulator, test_val);
            index = test_val;
          }
        }
        return accumulator;
    }

    @Benchmark
    public long randomizedBasicArrayLoopTest() {
        long accumulator = 0;
        int index = 0 ;
        final int length = encapsulatedArrayRandomized.getLength();
        if (SimpleMath) {
          for (int i = 0 ; i < length; i++) {
            int test_val= encapsulatedArrayRandomized.get(index).getTestValue();
            accumulator = MockStructure.simple_math (accumulator, test_val);
            index = test_val;
          }
        } else {
          for (int i = 0 ; i < length; i++) {
            int test_val= encapsulatedArrayRandomized.get(index).getTestValue();
            accumulator = MockStructure.some_math(accumulator, test_val);
            index = test_val;
          }
        }
        return accumulator;
    }

    public static class MockStructure {

        static final Class[] constructorArgTypes_Long_Bool_Int = {Long.TYPE, Boolean.TYPE, Integer.TYPE};
        static final Class[] constructorArgTypes_Int_Bool = {Integer.TYPE, Boolean.TYPE};
        static final Class[] constructorArgTypes_SA = {MockStructure.class};

        private long index = -1;
        private int testValue = Integer.MIN_VALUE;

        // padding elements - to make sure that each element is greater than 2x L1 D-cache line
        private long l0,l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15;

        public MockStructure() {
        }

        public MockStructure(final long index, final int testValue) {
            this.index = index;
            this.testValue = testValue;
        }

        public MockStructure(final long index, final boolean RandomArrayWalk, final int length) {
            this.index = index;
            if (RandomArrayWalk) {
                Random generator = new Random(index);
                this.testValue = generator.nextInt(length);
            } else {
                this.testValue = (int)index;
            }
       }

        public MockStructure(MockStructure src) {
            this.index = src.index;
            this.testValue = src.testValue;
        }

        public long getIndex() {
            return index;
        }

        public void setIndex(final long index) {
            this.index = index;
        }

        public int getTestValue() {
            return testValue;
        }

        public void setTestValue(final int testValue) {
            this.testValue = testValue;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            final MockStructure that = (MockStructure)o;

            return index == that.index && testValue == that.testValue;
        }

        @Override
        public int hashCode() {
            int result = (int)(index ^ (index >>> 32));
            result = 31 * result + (int)(testValue ^ (testValue >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return "MockStructure{" +
                    "index=" + index +
                    ", testValue=" + testValue +
                    '}';
        }

        public static long simple_math(long param1, int param2) {
            return  param1 ^ param2;
        }

        public static long some_math(long param1, int param2) {
             // Some random math
             // hopefully can get inlined
             // yet reduce data stalls

             long res, temp1, temp2, temp3, temp4;
             long l=param2;
             res= l & 0xFFF123;
             temp1 = l & 0xA9873;
             res = (res <<  (temp1 & 3))  + ((temp1 * 7)>> 1);
             temp2  = l & 0xFEDCBA;
             temp3  = l & 0xDCBA09;
             temp4  = l & 0x56789012;
             l=res + temp1 + temp2 + temp3 +temp4;

            res= l & 0xFFF123;
            temp1 = l & 0xA9873;
            res = (res <<  (temp1 & 3))  + ((temp1 * 7)>> 1);
            temp2  = l & 0xFEDCBA;
            temp3  = l & 0xDCBA09;
            temp4  = l & 0x56789012;
            l=res + temp1 + temp2 + temp3 +temp4;

            res= l & 0xFFF123;
            temp1 = l & 0xA9873;
            res = (res <<  (temp1 & 3))  + ((temp1 * 7)>> 1);
            temp2  = l & 0xFEDCBA;
            temp3  = l & 0xDCBA09;
            temp4  = l & 0x56789012;
            l=res + temp1 + temp2 + temp3 +temp4;

            res= l & 0xFFF123;
            temp1 = l & 0xA9873;
            res = (res <<  (temp1 & 3))  + ((temp1 * 7)>> 1);
            temp2  = l & 0xFEDCBA;
            temp3  = l & 0xDCBA09;
            temp4  = l & 0x56789012;
            l=res + temp1 + temp2 + temp3 +temp4;

            return param1 ^ l;
        }
    }

    public static class StructuredArrayOfMockStructure extends StructuredArray<MockStructure> {
        public static StructuredArrayOfMockStructure newInstance(
                final CtorAndArgsProvider<MockStructure> ctorAndArgsProvider,final long length) {
            return StructuredArray.newInstance(
                    StructuredArrayOfMockStructure.class, MockStructure.class, length, ctorAndArgsProvider);
        }

    }

    public static class MockStructureWith16PaddingLongs extends MockStructure {
        private long l0,l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15;

    }

    static class EncapsulatedArray {
        final MockStructure[] array;

        EncapsulatedArray(int length,boolean RandomArrayWalk) {
            array = new MockStructure[length];
            try {
                Constructor<MockStructure> constructor =
                        MockStructure.class.getConstructor(MockStructure.constructorArgTypes_Long_Bool_Int);
                for (int i = 0; i < array.length; i++) {
                    array[i] = constructor.newInstance(i, RandomArrayWalk, length);
                }
            } catch (Exception e) {}
        }

        EncapsulatedArray(StructuredArray<MockStructure> sa_array) {
            int length=(int)sa_array.getLength();
            final MockStructure[] a;
            a = new MockStructure[length];
            array = a;
            for (int i = 0; i < length; i++) {
                array[i] = sa_array.get(i);
            }
        }

        MockStructure get(final int index) {
            return array[index];
        }

        int getLength() {
            return array.length;
        }
    }

    static class EncapsulatedRandomizedArray {
        final MockStructure[] array;

        EncapsulatedRandomizedArray(int length,boolean RandomArrayWalk) {
            array = new MockStructure[length];
            Random generator = new Random(84);
            try {
                Constructor<MockStructure> constructor =
                        MockStructure.class.getConstructor(MockStructure.constructorArgTypes_Long_Bool_Int);
                for (int i = 0; i < array.length; i++) {
                    array[i] = constructor.newInstance(i, RandomArrayWalk, length);
                }
                generator = new Random(42);
                // swap elements around randomly
                for (int i = 0; i < array.length; i++) {
                    int target = generator.nextInt(array.length);
                    MockStructure temp = array[target];
                    array[target] = array[i];
                    array[i] = temp;
                }
            } catch (Exception e) {}

    }
        EncapsulatedRandomizedArray(StructuredArray<MockStructure> sa_array) {
            int length=(int)sa_array.getLength();
            final MockStructure[] a;
            a = new MockStructure[length];
            array = a;
            for (int i = 0; i < length; i++) {
                array[i] = sa_array.get(i);
            }
            // randomize
            Random generator = new Random(42);
            for (int i = 0; i < length; i++) {
                int target = generator.nextInt(array.length);
                array[i] = sa_array.get(target);
            }
        }

        MockStructure get(final int index) {
            return array[index];
        }

        int getLength() {
            return array.length;
        }
    }

    static class GenericEncapsulatedArray<E> {
        final E[] array;

        GenericEncapsulatedArray(Constructor<E> constructor, int length, boolean RandomArrayWalk)
                throws NoSuchMethodException {
            @SuppressWarnings("unchecked")
            final E[] a = (E[]) new Object[length];
            array = a;
            try {
                for (int i = 0; i < array.length; i++) {
                    array[i] = constructor.newInstance(i, RandomArrayWalk, length);
                }
            } catch (final Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        GenericEncapsulatedArray(Constructor<E> constructor, StructuredArray<E> sa_array) {
            int length = (int) sa_array.getLength();
            @SuppressWarnings("unchecked")
            final E[] a = (E[]) new Object[length];
            array = a;
            for (int i = 0; i < length; i++) {
                array[i] =  sa_array.get(i);
            }
        }

        E get(final int index) {
            return array[index];
        }

        int getLength() {
            return array.length;
        }

        public static void main(String[] args) throws RunnerException {

            Options opt = new OptionsBuilder()

                    .include(ObjectLayoutBench.class.getSimpleName())
                    .param("arg", "1048576") // Use this to selectively constrain/override parameters
                    .param("SameLayout", "true")
                    .param("RandomArrayWalk", "false")
                    .param("SimpleMath", "true")
                    .build();


            new Runner(opt).run();

        }
    }
}
