ifeq ($(OL_JAVA_HOME),)
$(error Maven will use variable OL_JAVA_HOME. Current value is not set)
endif

get_jdk: ./get-sources.sh
	@echo Fetch all jdk8udev sources with a special jdk/hotspot for ObjectLayout
	sh ./get-sources.sh

$(OL_JAVA_HOME)/bin/java:
	@echo Maven will use variable OL_JAVA_HOME. Current value is $(OL_JAVA_HOME)
	@echo if you have not build our custom jdk yet - go into jdk and do 'make images'
	@echo and set environment variable OL_JAVA_HOME accordingly
	exit 1

only_make: $(OL_JAVA_HOME)/bin/java
	@echo Maven will use variable OL_JAVA_HOME. Current value is $(OL_JAVA_HOME)
	mvn -Dmaven.test.skip=true compile

install_withhout_testing: $(OL_JAVA_HOME)/bin/java
	@echo Maven will use variable OL_JAVA_HOME. Current value is $(OL_JAVA_HOME)
	mvn -Dmaven.test.skip=true install

install: $(OL_JAVA_HOME)/bin/java
	@echo Maven will use variable OL_JAVA_HOME. Current value is $(OL_JAVA_HOME)
	mvn install

test: $(OL_JAVA_HOME)/bin/java
	@echo Testing using exclude list in file ./ObjectLayout/excluded_tests. This run should go clean
	@echo There tests are excluded
	@cat ./ObjectLayout/excluded_tests
	mvn test

test_with_known_fails: $(OL_JAVA_HOME)/bin/java
	mvn -Prun_all_tests,custom_jdk test

stock_jdk:
	echo test using a stock jdk like the one in JAVA_HOME
	mvn -Prun_all_tests,system_jdk test
