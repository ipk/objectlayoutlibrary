/*
 * Written by Gil Tene and Martin Thompson, and released to the public domain,
 * as explained at http://creativecommons.org/publicdomain/zero/1.0/
 */

package org.ObjectLayout;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import sun.misc.Unsafe;

/**
 * This class contains the intrinsified portions of StructuredArray behavior.
 *
 * @param <T> the element type in the array
 */
abstract class AbstractStructuredArray<T> {
    // The existence of this field (not its value) indicates that the class
    // should be intrinsified. (This allows the JVM to make this determination
    // at load time, and not wait for initialization.)
    private static final boolean existenceIndicatesIntrinsic = true;

    //
    // Instantiation and initialization
    //

    private boolean initialized = false;
    volatile boolean constructionCompleted = false;

    /**
     * Instantiate a StructuredArray with the given array model using
     * the supplied array constructor and arguments.
     */
    static <S extends AbstractStructuredArray<T>, T>
            S instantiateStructuredArray(
            AbstractStructuredArrayModel<S, T> arrayModel,
            Constructor<S> arrayCtor, Object[] arrayCtorArgs) {
        ConstructorMagic constructorMagic = getConstructorMagic();
        constructorMagic.setArrayModel(arrayModel);

        // Calculate array size in the heap
        long size = getArrayFootprint(arrayModel, false /* isContained */);

        try {
            constructorMagic.setActive(true);
            arrayCtor.setAccessible(true);
            Shape shape = new Shape(arrayModel);
            Object array = unsafe.allocateHeapForElementArrayClass(
                    arrayCtor.getDeclaringClass(),
                    shape.getElementCounts(), shape.getElementClasses());
            unsafe.constructObjectAtOffset(array, 0L /* offset */,
                    0L /* objectPrePadding */, false /* isContained */,
                    true /* isContainer */, size, arrayCtor, arrayCtorArgs);
            return (S) array;
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        } finally {
            constructorMagic.setActive(false);
        }
    }

    AbstractStructuredArray() {
        checkConstructorMagic();
        ConstructorMagic constructorMagic = getConstructorMagic();

        @SuppressWarnings("unchecked")
        final AbstractStructuredArrayModel<AbstractStructuredArray<T>, T>
                arrayModel = constructorMagic.getArrayModel();

        // Finish consuming constructorMagic arguments
        constructorMagic.setActive(false);

        if (arrayModel._getLength() < 0) {
            throw new IllegalArgumentException("Length cannot be negative.");
        }

        // Initialize hidden fields
        initBodySize((int) unsafe.getInstanceSize(this.getClass()));
        initLength(arrayModel._getLength());
        final long elementSize = getElementFootprint(arrayModel);
        initElementSize(elementSize);
        initPaddingSize(unsafe.getPrePaddingInObjectFootprint(elementSize));
        initElementClass(arrayModel._getElementClass());

        // Indicate construction is complete, such that further calls to
        // initX() initializers of hidden fields will fail from this point on
        initialized = true;

        // Follow update of internal boolean indication with a modification
        // of a package-visible volatile to ensure ordering (this way
        // initialized does not have to be volatile and normal accessor
        // actions do not take the penalty of a volatile read barrier)
        constructionCompleted = true;
    }

    private static long getArrayFootprint(
            AbstractStructuredArrayModel arrayModel,
            boolean isContained) {
        long footprint = isContained ?
                unsafe.getContainingObjectFootprintWhenContained(
                        arrayModel._getArrayClass(),
                        getElementFootprint(arrayModel),
                        arrayModel._getLength()
                ) :
                unsafe.getContainingObjectFootprint(
                        arrayModel._getArrayClass(),
                        getElementFootprint(arrayModel),
                        arrayModel._getLength()
                );
        return footprint;
    }

    private static long getElementFootprint(
            AbstractStructuredArrayModel arrayModel) {
        long footprint;
        if (arrayModel._getStructuredSubArrayModel() != null) {
            // Element is a structured array.
            footprint = getArrayFootprint(
                    arrayModel._getStructuredSubArrayModel(),
                    true /* isContained */);
        } else if (arrayModel._getPrimitiveSubArrayModel() != null) {
            // Element is a primitive array.
            AbstractPrimitiveArrayModel subArrayModel =
                    arrayModel._getPrimitiveSubArrayModel();
            @SuppressWarnings("unchecked")
            Class<? extends AbstractPrimitiveArray> subArrayClass =
                    subArrayModel._getArrayClass();
            long subArrayLength = subArrayModel._getLength();
            footprint = AbstractPrimitiveArray.getPrimitiveArrayFootprint(
                    subArrayClass, subArrayLength, true /* isContained */);
        } else {
            // Element is a regular object.
            footprint = unsafe.getInstanceFootprintWhenContained(
                    arrayModel._getElementClass());
        }
        return footprint;
    }

    /**
     * Construct a fresh element intended to occupy a given index in this
     * StructuredArray instance, using the supplied constructor and arguments.
     */
    void constructElementAtIndex(final long index,
            final Constructor<T> constructor, final Object[] args) {
        if ((index < 0) || (index >= getLength())) {
            throw new ArrayIndexOutOfBoundsException();
        }

        try {
            long offset = getBodySize() + index * getElementSize() +
                    getPaddingSize();
            unsafe.constructObjectAtOffset(this, offset, getPaddingSize(),
                    true /* isContained */, false /* isContainer */,
                    getElementSize(), constructor, args);
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Construct a fresh primitive sub-array intended to occupy a given index
     * in this StructuredArray instance, using the supplied constructor and
     * arguments.
     */
    void constructPrimitiveSubArrayAtIndex(
            final long index,
            AbstractPrimitiveArrayModel primitiveSubArrayModel,
            final Constructor<T> constructor,
            final Object... args) {
        if ((index < 0) || (index >= getLength())) {
            throw new ArrayIndexOutOfBoundsException();
        }

        int length = (int) primitiveSubArrayModel._getLength();

        // In this path of execution we have not installed yet the length
        // of the PrimitiveArray into TLS's threadLocalConstructorMagic for PrimitiveArrays
        // This is what the stack trace may looks like
        //at org.ObjectLayout.AbstractPrimitiveLongArray.<init>(AbstractPrimitiveLongArray.java:75)
        //at org.ObjectLayout.PrimitiveLongArray.<init>(PrimitiveLongArray.java:87)
        //at org.ObjectLayout.PrimitiveLongArrayTest$ColoredLongArray.<init>(PrimitiveLongArrayTest.java:31)
        //at sun.misc.Unsafe.constructObjectAtOffset(Native Method)
        //at org.ObjectLayout.AbstractStructuredArray.constructElementAtIndex(AbstractStructuredArray.java:159)
        //at org.ObjectLayout.AbstractStructuredArray.constructPrimitiveSubArrayAtIndex(AbstractStructuredArray.java:195)
        //at org.ObjectLayout.StructuredArray.populatePrimitiveSubArray(StructuredArray.java:557)
        //at org.ObjectLayout.StructuredArray.populatePrimitiveSubArrays(StructuredArray.java:626)
        //at org.ObjectLayout.StructuredArray.<init>(StructuredArray.java:477)
        //at sun.misc.Unsafe.constructObjectAtOffset(Native Method)
        //at org.ObjectLayout.AbstractStructuredArray.instantiateStructuredArray(AbstractStructuredArray.java:51)
        //at org.ObjectLayout.StructuredArray.instantiate(StructuredArray.java:442)
        //at org.ObjectLayout.StructuredArray.newInstance(StructuredArray.java:270)
        //at org.ObjectLayout.StructuredArrayBuilder.build(StructuredArrayBuilder.java:347)
        //at test
        AbstractPrimitiveArray.installPrimitiveArrayLength(length);
        // The following is 'almost' correct
        // constructElementAtIndex will create an object marked as non-container
        // That is true for now because we have not intrinsified PrimitiveArrays yet
        // Therefore instances of PrimitiveArrays themselves are just regular java object references
        // TODO: FIXME: once we add support for PrimitiveArrays - we should construct those
        // slightly differently
        constructElementAtIndex(index, constructor, args);
    }

    /**
     * Construct a fresh sub-array intended to occupy a given index in this
     * StructuredArray instance, using the supplied constructor and arguments.
     */
    void constructSubArrayAtIndex(final long index,
            final AbstractStructuredArrayModel subArrayModel,
            final Constructor<T> arrayCtor, final Object[] arrayCtorArgs) {
        if ((index < 0) || (index >= getLength())) {
            throw new ArrayIndexOutOfBoundsException();
        }

        ConstructorMagic constructorMagic = getConstructorMagic();
        constructorMagic.setArrayModel(subArrayModel);
        try {
            constructorMagic.setActive(true);
            arrayCtor.setAccessible(true);
            long offset = getBodySize() + index * getElementSize() +
                    getPaddingSize();
            unsafe.constructObjectAtOffset(this, offset, getPaddingSize(),
                    true /* isContained */, true /* isContainer */,
                    getElementSize(), arrayCtor, arrayCtorArgs);
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        } finally {
            constructorMagic.setActive(false);
        }
    }

    /**
     * Construct a fresh StructuredArray intended to occupy a given intrinsic field in the containing object,
     * at the field described by the supplied intrinsicObjectModel, using the supplied constructor and arguments.
     *
     * OPTIMIZATION NOTE: Optimized JDK implementations may replace this implementation with a
     * construction-in-place call on a previously allocated memory location associated with the given index.
     */
    static <T> T constructStructuredArrayWithin(
            final Object containingObject,
            final AbstractIntrinsicObjectModel<T> intrinsicObjectModel,
            AbstractStructuredArrayModel subArrayModel,
            final Constructor<T> subArrayConstructor,
            final Object... args) {
        ConstructorMagic constructorMagic = getConstructorMagic();
        constructorMagic.setArrayModel(subArrayModel);

        // Calculate array size in the heap
        long size = getArrayFootprint(subArrayModel, false /* isContained */);

        try {

            // This is a HACK
            // TODO: FIXME
            // We currently do not support instantiation mechanism for Intrinsified Objects
            // We got to this method because some class with Intrinsified Arrays has a StructuredArray as a field
            // In a true Intrinsified implementation the space for such StructuredArray should have been allocated
            // This is not the case yet.
            //
            // Therefore we allocate this StructuredArray in a heap as a new "free standing" object
            // and return it

            constructorMagic.setActive(true);
            Shape shape = new Shape(subArrayModel);
            Object array = unsafe.allocateHeapForElementArrayClass(
                    subArrayConstructor.getDeclaringClass(),
                    shape.getElementCounts(), shape.getElementClasses());
            unsafe.constructObjectAtOffset(array, 0L /* offset */,
                    0L /* objectPrePadding */, false /* isContained */,
                    true /* isContainer */, size, subArrayConstructor, args);
            return (T) array;

        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        } finally {
            constructorMagic.setActive(false);
        }
    }

    /**
     * Get an element at a supplied index in this StructuredArray instance.
     */
    T get(final int index) {
        if ((index < 0) || (index >= getLength())) {
            throw new ArrayIndexOutOfBoundsException();
        }

        long offset = getBodySize() + index * getElementSize() +
                getPaddingSize();
        return (T) unsafe.deriveContainedObjectAtOffset(this, offset);
    }

    /**
     * Get an element at a supplied index in this StructuredArray instance.
     */
    T get(final long index) {
        if ((index < 0) || (index >= getLength())) {
            throw new ArrayIndexOutOfBoundsException();
        }

        long offset = getBodySize() + index * getElementSize() +
                getPaddingSize();
        return (T) unsafe.deriveContainedObjectAtOffset(this, offset);
    }

    //
    // Instance state
    //

    private int bodySize;
    private long length;
    private long elementSize;
    private long paddingSize;
    private Class<T> elementClass;

    private int getBodySize() {
        return bodySize;
    }

    private void initBodySize(int bodySize) {
        if (initialized) {
            throw new IllegalArgumentException(
                    "Cannot change value after construction.");
        }
        this.bodySize = bodySize;
    }

    // This method needs to be package-private.
    long getLength() {
        return length;
    }

    private void initLength(long length) {
        if (initialized) {
            throw new IllegalArgumentException(
                    "Cannot change value after construction.");
        }
        this.length = length;
    }

    private long getElementSize() {
        return elementSize;
    }

    private void initElementSize(long elementSize) {
        if (initialized) {
            throw new IllegalArgumentException(
                    "Cannot change value after construction.");
        }
        this.elementSize = elementSize;
    }

    private long getPaddingSize() {
        return paddingSize;
    }

    private void initPaddingSize(long paddingSize) {
        if (initialized) {
            throw new IllegalArgumentException(
                    "Cannot change value after construction.");
        }
        this.paddingSize = paddingSize;
    }

    // This method needs to be package-private.
    Class<T> getElementClass() {
        return elementClass;
    }

    private void initElementClass(Class<T> elementClass) {
        if (initialized) {
            throw new IllegalArgumentException(
                    "Cannot change value after construction.");
        }
        this.elementClass = elementClass;
    }

    //
    // ConstructorMagic support
    //

    private static class ConstructorMagic {
        private boolean active = false;
        private AbstractStructuredArrayModel arrayModel;

        private boolean isActive() {
            return active;
        }

        private void setActive(boolean active) {
            this.active = active;
        }

        private AbstractStructuredArrayModel getArrayModel() {
            return arrayModel;
        }

        private void setArrayModel(AbstractStructuredArrayModel arrayModel) {
            this.arrayModel = arrayModel;
        }
    }

    private static final ThreadLocal<ConstructorMagic>
            threadLocalConstructorMagic = new ThreadLocal<ConstructorMagic>() {
                @Override
                protected ConstructorMagic initialValue() {
                    return new ConstructorMagic();
                }
            };

    private static ConstructorMagic getConstructorMagic() {
        return threadLocalConstructorMagic.get();
    }

    private static void checkConstructorMagic() {
        if (!getConstructorMagic().isActive()) {
            throw new IllegalArgumentException(
                    "StructuredArray must not be directly instantiated " +
                    "with a constructor. Use StructuredArray.newInstance() " +
                    "instead.");
        }
    }

    //
    // Adaptation of AbstractStructuredArrayModel used to do calls to
    // Unsafe.allocateHeapForElementArrayClass()
    //

    private static class Shape {
        private long[] elementCounts;
        private Class[] elementClasses;

        private Shape(AbstractStructuredArrayModel arrayModel) {
            if (arrayModel == null) {
                throw new IllegalArgumentException(
                        "arrayModel cannot be null.");
            }

            int dims = 0;
            AbstractStructuredArrayModel model = arrayModel;
            do {
                dims++;
                model = model._getStructuredSubArrayModel();
            } while (model != null);

            elementCounts = new long[dims];
            elementClasses = new Class[dims];

            int i = 0;
            model = arrayModel;
            do {
                elementCounts[i] = model._getLength();
                elementClasses[i] = model._getElementClass();
                i++;
                model = model._getStructuredSubArrayModel();
            } while (model != null);
        }

        private long[] getElementCounts() {
            return elementCounts;
        }

        private Class[] getElementClasses() {
            return elementClasses;
        }
    }

    //
    // Local support for Unsafe operations
    //

    private static final Unsafe unsafe;
    static {
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            unsafe = (Unsafe) field.get(null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //
    // Debugging support
    //

    @Override
    public String toString() {
        final StringBuilder output =
                new StringBuilder("AbstractStructuredArray");
        output.append("<").append(getElementClass().getName()).append(">");
        output.append("[").append(getLength()).append("]");
        output.append(": bodySize = ").append(getBodySize());
        output.append(", elementSize = ").append(getElementSize());
        output.append(", paddingSize = ").append(getPaddingSize());
        return new String(output);
    }
}
